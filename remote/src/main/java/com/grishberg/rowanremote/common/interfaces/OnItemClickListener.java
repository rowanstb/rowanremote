package com.grishberg.rowanremote.common.interfaces;

/**
 * Created by grishberg on 15.05.16.
 */
public interface OnItemClickListener {
    /**
     * Реакция на нажатие по элементу списка
     *
     * @param id           идентификатор элемента в бд
     * @param offsetTop    верхнее смещение в пикселях
     * @param offsetBottom нижнее смещение в пикселях
     */
    void onItemClicked(
            long id,
            int offsetTop,
            int offsetBottom);
}
