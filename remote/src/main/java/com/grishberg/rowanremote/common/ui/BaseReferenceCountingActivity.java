package com.grishberg.rowanremote.common.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.grishberg.rowancommon.ui.activities.BaseActivity;
import com.grishberg.rowanremote.App;
import com.grishberg.rowanremote.data.networking.RcService;

/**
 * Created by grishberg on 20.05.16.
 */
public class BaseReferenceCountingActivity extends BaseActivity<RcService> {
    private static final String TAG = BaseReferenceCountingActivity.class.getSimpleName();

    @Override
    protected Intent getServiceIntent() {
        return new Intent(this, RcService.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.activityCreated();
        startService(bindServiceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.activityDestroyed();
        if (App.isExitingFromApp()) {
            stopService(bindServiceIntent);
        }
    }
}
