package com.grishberg.rowanremote.common.interfaces;

import com.grishberg.rowancommon.interfaces.FragmentsRegistrable;

/**
 * Created by grishberg on 15.05.16.
 */
public interface DevicesInteractor<T> extends FragmentsRegistrable<T> {
    void onDeviceSelected(String address);
}
