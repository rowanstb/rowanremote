package com.grishberg.rowanremote.data.models;

import android.support.annotation.NonNull;

/**
 * Created by grishberg on 24.06.16.
 */
public class ServerInfoContainer {
    private static final String TAG = ServerInfoContainer.class.getSimpleName();
    private String address;
    private String name;

    public ServerInfoContainer(@NonNull String address, String name) {
        this.address = address;
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof ServerInfoContainer && address != null) {
            return address.equals(((ServerInfoContainer) o).address);
        }
        return false;
    }

    @Override
    public String toString() {
        return "ServerInfoContainer{" +
                "address='" + address + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
