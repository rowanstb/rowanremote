package com.grishberg.rowanremote.data.dispatchers;

/**
 * Created by grishberg on 16.05.16.
 */
public class ConnectionDispatcherImpl implements ConnectionDispatcher {
    private static final String TAG = ConnectionDispatcherImpl.class.getSimpleName();
    private static ConnectionDispatcher sInstance;

    public static ConnectionDispatcher getInstance() {
        if (sInstance == null) {
            sInstance = new ConnectionDispatcherImpl();
        }
        return sInstance;
    }

    private volatile String connectedDevice;

    @Override
    public void connectToDevice(String address) {
        connectedDevice = address;
    }

    @Override
    public void disconnect() {
        connectedDevice = null;
    }

    @Override
    public String getConnectedDevice() {
        return connectedDevice;
    }
}
