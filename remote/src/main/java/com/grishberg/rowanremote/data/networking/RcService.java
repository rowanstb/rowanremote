package com.grishberg.rowanremote.data.networking;

import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.grishberg.rowancommon.NetworkingConst;
import com.grishberg.rowancommon.data.ParcelableConverter;
import com.grishberg.rowancommon.data.models.MessagesContainer;
import com.grishberg.rowancommon.data.services.HandlerReceiverImpl;
import com.grishberg.rowanremote.data.dispatchers.ConnectionDispatcher;
import com.grishberg.rowanremote.data.dispatchers.ConnectionDispatcherImpl;
import com.grishberg.rowanremote.data.models.ServerInfoContainer;
import com.grishberg.utils.network.ServerFinder;
import com.grishberg.utils.network.ServerFinderImpl;
import com.grishberg.utils.network.interfaces.OnCloseConnectionListener;
import com.grishberg.utils.network.interfaces.OnConnectionErrorListener;
import com.grishberg.utils.network.interfaces.OnFinderConnectionEstablishedListener;
import com.grishberg.utils.network.interfaces.OnMessageListener;
import com.grishberg.utils.network.interfaces.OnServerConnectionEstablishedListener;
import com.grishberg.utils.network.tcp.client.TcpClient;
import com.grishberg.utils.network.tcp.client.TcpClientImpl;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RcService extends HandlerReceiverImpl implements OnMessageListener,
        OnConnectionErrorListener,
        OnServerConnectionEstablishedListener, OnFinderConnectionEstablishedListener, OnCloseConnectionListener {
    private static final String TAG = RcService.class.getSimpleName();
    public static final int ACTION_FOUND_SERVER = 1;
    public static final int ACTION_CONNECTED_TO_SERVER = 2;
    public static final int ACTION_MESSAGE = 3;
    public static final int ACTION_CONNECTION_ERROR = -1;

    private Map<String, TcpClient> tcpClients;
    private ServerFinder serverFinder;
    private ConnectionDispatcher connectionDispatcher = ConnectionDispatcherImpl.getInstance();
    private byte[] notSentPacket;

    public RcService() {
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: " + this);
        super.onCreate();
        serverFinder = new ServerFinderImpl(NetworkingConst.BROADCAST_PORT, NetworkingConst.BACK_TCP_PORT);
        tcpClients = new ConcurrentHashMap<>();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: " + this);
        super.onDestroy();
        stopListeningServers();
        disconnect();
    }

    public void startListeningServers() {
        Log.d(TAG, "startListeningServers: ");
        serverFinder.setConnectionListener(this);
        serverFinder.startListeningServers();
    }

    public void findServers() {
        Log.d(TAG, "findServers: ");
        serverFinder.findServer();
    }

    public void stopListeningServers() {
        Log.d(TAG, "stopListeningServers: ");
        if (serverFinder != null) {
            serverFinder.stopListening();
        }
    }

    public void connectToServer(final String address) {
        Log.d(TAG, "connectToServer: address = " + address);
        try {
            disconnect();
            TcpClient client = tcpClients.get(address);
            if (client == null) {
                Log.d(TAG, "connectToServer: create new tcp client");
                client = new TcpClientImpl(this, this, this, this);
                tcpClients.put(address, client);
            }
            new AsyncTask<TcpClient, Void, Void>() {
                @Override
                protected Void doInBackground(TcpClient... params) {
                    Log.d(TAG, "doInBackground: connecting...");
                    TcpClient client = params[0];
                    client.connect(address, NetworkingConst.TCP_PORT);
                    Log.d(TAG, "doInBackground: connected");
                    // если есть неотправленный пакет - оптравить
                    if (notSentPacket != null) {
                        client.sendMessage(notSentPacket);
                        notSentPacket = null;
                    }
                    return null;
                }
            }.execute(client);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Отключиться от текущего подключенного устройства
     */
    public void disconnect() {
        Log.d(TAG, "disconnect: ");
        String address = connectionDispatcher.getConnectedDevice();
        if (address != null) {
            TcpClient client = tcpClients.get(address);
            if (client != null) {
                Log.d(TAG, "disconnect: release address = " + address);
                client.release();
            }
            connectionDispatcher.disconnect();
        }
    }

    public void sendMessage(@NonNull String address, Parcelable cmd) {
        Log.d(TAG, "sendMessage: to " + address);
        byte[] packet = ParcelableConverter.convertToArray(cmd);

        TcpClient client = tcpClients.get(address);
        if (client != null) {
            client.sendMessage(packet);
        } else {
            Log.e(TAG, "sendMessage: client is null for " + address + " try to connect");
            notSentPacket = packet;
            connectToServer(address);
        }
    }

    /**
     * Событие при входящем сообщении
     *
     * @param address
     * @param bytes
     */
    @Override
    public void onReceivedMessage(String address, byte[] bytes) {
        Log.d(TAG, "onReceivedMessage: from " + address);
        sendMessageToActivity(new MessagesContainer(ACTION_MESSAGE, ParcelableConverter.convertFromArray(bytes)));
    }

    @Override
    public void onConnectionEstablished(String address) {
        connectionDispatcher.connectToDevice(address);
        sendMessageToActivity(new MessagesContainer(ACTION_CONNECTED_TO_SERVER, address));
    }

    @Override
    public void onError(Throwable throwable) {
        sendMessageToActivity(new MessagesContainer(ACTION_CONNECTION_ERROR, throwable));
    }

    /**
     * Событие отклика сервера
     * @param address
     * @param serverName
     */
    @Override
    public void onServerFound(String address, String serverName) {
        sendMessageToActivity(new MessagesContainer(ACTION_FOUND_SERVER,
                new ServerInfoContainer(address, serverName)));
    }

    @Override
    public void onCloseConnection(String address) {
        Log.d(TAG, "onCloseConnection: " + address);
        if (address != null) {
            tcpClients.remove(address);
        }
    }
}
