package com.grishberg.rowanremote.data.dispatchers;

/**
 * Created by grishberg on 16.05.16.
 */
public interface ConnectionDispatcher {
    void connectToDevice(String address);
    void disconnect();
    String getConnectedDevice();
}
