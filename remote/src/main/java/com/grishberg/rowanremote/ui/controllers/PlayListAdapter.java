package com.grishberg.rowanremote.ui.controllers;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackRelation;
import com.grishberg.rowanremote.R;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by grishberg on 20.06.16.
 */
public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.TrackViewHolder>
        implements ItemTouchHelperAdapter {
    private static final String TAG = PlayListAdapter.class.getSimpleName();
    private View.OnClickListener onItemClickListener;
    private List<TrackRelation> trackList;
    private int currentTrackId;
    private final OnStartDragListener mDragStartListener;

    public PlayListAdapter(OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
    }

    public void setOnItemClickListener(View.OnClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setTrackList(List<TrackRelation> trackList) {
        this.trackList = trackList;
        notifyDataSetChanged();
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_track_cell, parent, false);
        return new TrackViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(TrackViewHolder holder, int position) {
        holder.bind(trackList.get(position).getTrackItem(), currentTrackId);
    }

    @Override
    public int getItemCount() {
        return trackList != null ? trackList.size() : 0;
    }

    public void setCurrentTrackId(int trackId) {
        currentTrackId = trackId;
        notifyDataSetChanged();
    }

    // Drag and Drop

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        TrackRelation targetItem = trackList.get(fromPosition);
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(trackList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(trackList, i, i - 1);
            }
        }

        mDragStartListener.onSwap(targetItem.getTrackItem().getId(), toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        trackList.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * ViewHolder
     */
    public class TrackViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {
        @Bind(R.id.tvName)
        TextView name;

        @Bind(R.id.tvArtist)
        TextView artist;

        @Bind(R.id.llBackground)
        View backgroung;

        public TrackViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(onItemClickListener);
            // событие длинного удерживания для драга
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mDragStartListener.onStartDrag(TrackViewHolder.this);
                    return false;
                }
            });
        }

        public void bind(TrackItem trackItem, int currentTrackId) {
            name.setText(trackItem.getName());
            artist.setText(trackItem.getArtist());
            itemView.setTag(trackItem);
            if (trackItem.getId() == currentTrackId) {
                backgroung.setVisibility(View.VISIBLE);
                return;
            }
            backgroung.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}
