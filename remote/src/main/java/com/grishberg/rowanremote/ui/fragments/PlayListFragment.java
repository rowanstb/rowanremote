package com.grishberg.rowanremote.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.grishberg.rowancommon.data.models.commands.InfoCmd;
import com.grishberg.rowancommon.data.models.commands.PlayListCmd;
import com.grishberg.rowancommon.data.models.commands.PlayerCmd;
import com.grishberg.rowancommon.interfaces.BoundListener;
import com.grishberg.rowancommon.interfaces.HandlerReceiver;
import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackRelation;
import com.grishberg.rowanremote.R;
import com.grishberg.rowanremote.common.interfaces.DevicesInteractor;
import com.grishberg.rowanremote.data.networking.RcService;
import com.grishberg.rowanremote.ui.controllers.OnStartDragListener;
import com.grishberg.rowanremote.ui.controllers.PlayListAdapter;
import com.grishberg.rowanremote.ui.controllers.SimpleItemTouchHelperCallback;
import com.grishberg.rowanremote.ui.views.PlayListView;

import java.util.List;

/**
 * Created by grishberg on 20.06.16.
 */
public class PlayListFragment extends Fragment implements PlayListView, BoundListener,
        OnStartDragListener {
    private static final String TAG = PlayListFragment.class.getSimpleName();
    public static final String ARG_ADDRESS = "argAddress";

    RecyclerView recyclerView;

    private PlayListAdapter adapter;
    private DevicesInteractor<RcService> mListener;
    private String controlledDevice;
    private ItemTouchHelper mItemTouchHelper;

    public static PlayListFragment newInstance(String controlledDevice) {
        PlayListFragment fragment = new PlayListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ADDRESS, controlledDevice);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DevicesInteractor) {
            mListener = (DevicesInteractor) context;
            mListener.register(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            controlledDevice = getArguments().getString(ARG_ADDRESS);
        }
        adapter = new PlayListAdapter(this);
        adapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TrackItem trackItem = (TrackItem) view.getTag();
                Log.d(TAG, "onClick: " + trackItem);
                if (trackItem == null) {
                    return;
                }
                if (TextUtils.isEmpty(controlledDevice)) {
                    Log.e(TAG, "onClick: device is empty");
                    Toast.makeText(PlayListFragment.this.getActivity(),
                            R.string.no_device, Toast.LENGTH_SHORT).show();
                    return;
                }
                // Воспроизвести выбранный трек
                mListener.getService().sendMessage(controlledDevice,
                        new PlayerCmd(PlayerCmd.CMD_PLAY, trackItem.getId()));
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        return inflater.inflate(R.layout.fragment_playlist, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: ");
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvTracks);
        recyclerView.setVisibility(View.VISIBLE);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onSwap(int fromId, int toPosition) {
        if (mListener.getService() != null && controlledDevice != null) {
            Log.d(TAG, "onSwap: fromId=" + fromId);
            mListener.getService().sendMessage(controlledDevice,
                    new InfoCmd(0, InfoCmd.SWAP_TRACK, fromId, toPosition));
        }
    }

    private void makeRequest() {
        if (mListener.getService() != null && controlledDevice != null) {
            Log.d(TAG, "makeRequest: addr=" + controlledDevice);
            mListener.getService().sendMessage(controlledDevice,
                    new InfoCmd(0, InfoCmd.GET_CURRENT_PLAYLIST));
            mListener.getService().sendMessage(controlledDevice,
                    new InfoCmd(0, InfoCmd.GET_CURRENT_TRACK));
        }
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView: ");
        super.onDestroyView();
    }

    @Override
    public void onServiceBound(HandlerReceiver service) {
        Log.d(TAG, "onServiceBound: ");
    }

    @Override
    public void onServiceUnBound(HandlerReceiver service) {

    }

    @Override
    public void onMessageReceived(int what, Object obj) {
        Log.d(TAG, "onMessageReceived: " + what);
        if (what != RcService.ACTION_MESSAGE) {
            return;
        }
        Parcelable msg = (Parcelable) obj;
        if (msg instanceof PlayListCmd) {
            PlayListCmd playListCmd = (PlayListCmd) obj;
            Log.d(TAG, "onMessageReceived: get playlist");
            resolveTitle(playListCmd.getName());
            setTracks(playListCmd.getTrackList());
            return;
        }
        if (msg instanceof InfoCmd && ((InfoCmd) msg).getType() == InfoCmd.CURRENT_TRACK) {
            Log.d(TAG, "onMessageReceived: CURRENT_TRACK " + ((InfoCmd) msg).getDataInt());
            setCurrentTrack(((InfoCmd) msg).getDataInt());
        }
    }

    @Override
    public void resolveTitle(String title) {
        //TODO:
    }

    @Override
    public void setTracks(List<TrackRelation> trackList) {
        adapter.setTrackList(trackList);
    }

    @Override
    public void setCurrentTrack(int trackId) {
        adapter.setCurrentTrackId(trackId);
    }

    public void setControlledDevice(String controlledDevice) {
        Log.d(TAG, "setControlledDevice() called with: " + "controlledDevice = [" + controlledDevice + "]");
        this.controlledDevice = controlledDevice;
        makeRequest();
    }
}
