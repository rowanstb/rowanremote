package com.grishberg.rowanremote.ui.controllers;

/**
 * Created by grishberg on 22.06.16.
 */
public interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
