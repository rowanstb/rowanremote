package com.grishberg.rowanremote.ui.views;

import com.grishberg.datafacade.data.ListResult;
import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackList;
import com.grishberg.rowancommon.mediaPlayer.TrackRelation;

import java.util.List;

/**
 * Created by grishberg on 21.06.16.
 */
public interface PlayListView {
    void resolveTitle(String title);

    void setTracks(List<TrackRelation> list);

    void setCurrentTrack(int trackId);
}
