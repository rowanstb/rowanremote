package com.grishberg.rowanremote.ui.controllers;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grishberg.rowanremote.R;
import com.grishberg.rowanremote.common.interfaces.OnItemClickListener;
import com.grishberg.rowanremote.data.models.ServerInfoContainer;

import java.util.List;

/**
 * Created by grishberg on 15.05.16.
 */
public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.DeviceViewHolder> {
    private static final String TAG = DevicesAdapter.class.getSimpleName();
    private List<ServerInfoContainer> devices;
    private final OnItemClickListener listener;

    public DevicesAdapter(List<ServerInfoContainer> devices, OnItemClickListener listener) {
        this.listener = listener;
        this.devices = devices;
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_device_cell
                , parent, false);
        DeviceViewHolder vh = new DeviceViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder holder, int position) {
        ServerInfoContainer item = getItem(position);
        holder.bind(position, item);
    }

    private ServerInfoContainer getItem(int position) {
        return devices.get(position);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder {
        int id;
        TextView tvDeviceAddress;
        TextView tvDeviceName;
        OnItemClickListener clickListener;

        public DeviceViewHolder(View itemView) {
            super(itemView);
            tvDeviceAddress = (TextView) itemView.findViewById(R.id.tvDeviceAddress);
            tvDeviceName = (TextView) itemView.findViewById(R.id.tvDeviceName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        int offsetTop = v.getTop();
                        int offsetBottom = v.getBottom();
                        Log.d(TAG, String.format("onClick: offsetTop = %d, offsetBottom = %d",
                                offsetTop, offsetBottom));
                        clickListener.onItemClicked(id, offsetTop, offsetBottom);
                    }
                }
            });
        }

        public void bind(int position, ServerInfoContainer serverInfoContainer) {
            tvDeviceAddress.setText(serverInfoContainer.getAddress());
            tvDeviceName.setText(serverInfoContainer.getName());
            id = position;
            clickListener = listener;
        }
    }
}
