package com.grishberg.rowanremote.ui.controllers;

import android.support.v7.widget.RecyclerView;

/**
 * Created by grishberg on 22.06.16.
 */
public interface OnStartDragListener {
    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);

    /**
     * Уведомить о перемещении трэка
     * @param fromId
     * @param toPosition
     */
    void onSwap(int fromId, int toPosition);
}
