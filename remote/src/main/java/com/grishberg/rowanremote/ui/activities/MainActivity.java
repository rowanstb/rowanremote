package com.grishberg.rowanremote.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.grishberg.rowancommon.data.FileUtils;
import com.grishberg.rowancommon.data.models.commands.SendFileCmd;
import com.grishberg.rowanremote.R;
import com.grishberg.rowanremote.common.interfaces.DevicesInteractor;
import com.grishberg.rowanremote.common.ui.BaseReferenceCountingActivity;
import com.grishberg.rowanremote.data.networking.RcService;
import com.grishberg.rowanremote.ui.fragments.DevicesFragment;
import com.grishberg.rowanremote.ui.fragments.PlayListFragment;
import com.grishberg.rowanremote.ui.fragments.RemoteFragment;

import java.io.File;

public class MainActivity extends BaseReferenceCountingActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        DevicesInteractor<RcService> {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int SHARE_TYPE_IMAGE = 1;
    private static final int SHARE_TYPE_AUDIO = 2;
    public static final String CONTROLLED_DEVICE_PARAM = "controlledDeviceParam";
    private String controlledDevice;
    private Intent shareIntent;
    private int shareType = -1;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: " + savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (savedInstanceState == null) {
            showDevicesFragment(getSupportFragmentManager());
        } else {
            controlledDevice = savedInstanceState.getString(CONTROLLED_DEVICE_PARAM);
        }
        shareIntent = getIntent();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CONTROLLED_DEVICE_PARAM, controlledDevice);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent: ");
        super.onNewIntent(intent);
        shareIntent = intent;
    }

    @Override
    protected void onBound() {
        Log.d(TAG, "onBound: ");
        super.onBound();
        if (shareIntent != null) {
            if (controlledDevice != null) {
                processIntent(shareIntent);
                shareIntent = null;
                return;
            }
            showDevicesFragment(getSupportFragmentManager());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Реакция на выбор пунктов меню
     *
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = null;
        if (id == R.id.nav_devices) {
            showDevicesFragment(fragmentManager);
        } else if (id == R.id.nav_remote) {
            showRemoteFragment(fragmentManager);
        } else if (id == R.id.nav_playlist) {
            showPlayListFragment(fragmentManager);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showDevicesFragment(FragmentManager fragmentManager) {
        FragmentTransaction transaction;
        transaction = fragmentManager.beginTransaction();
        DevicesFragment devicesFragment = (DevicesFragment) fragmentManager
                .findFragmentByTag(DevicesFragment.class.getSimpleName());
        if (devicesFragment == null) {
            devicesFragment = DevicesFragment.newInstance();
        }
        transaction.replace(R.id.contentMain, devicesFragment);
        transaction.commitAllowingStateLoss();
    }

    private void showRemoteFragment(FragmentManager fragmentManager) {
        FragmentTransaction transaction;
        transaction = fragmentManager.beginTransaction();
        RemoteFragment remoteFragment = (RemoteFragment) fragmentManager
                .findFragmentByTag(RemoteFragment.class.getSimpleName());
        if (remoteFragment == null) {
            remoteFragment = RemoteFragment.newInstance();
        }
        transaction.replace(R.id.contentMain, remoteFragment);
        transaction.commit();
        fragmentManager.executePendingTransactions();
        remoteFragment.setControlledDevice(controlledDevice);
    }

    /**
     * Отобразить плейлист
     *
     * @param fragmentManager
     */
    private void showPlayListFragment(FragmentManager fragmentManager) {
        FragmentTransaction transaction;
        transaction = fragmentManager.beginTransaction();
        PlayListFragment remoteFragment = (PlayListFragment) fragmentManager
                .findFragmentByTag(PlayListFragment.class.getSimpleName());
        if (remoteFragment == null) {
            remoteFragment = PlayListFragment.newInstance(controlledDevice);
        }
        transaction.replace(R.id.contentMain, remoteFragment);
        transaction.commit();
        fragmentManager.executePendingTransactions();
        remoteFragment.setControlledDevice(controlledDevice);
    }

    /**
     * Событие возникает когда в диалоге выбора устройства кликнули на приставку
     *
     * @param address
     */
    @Override
    public void onDeviceSelected(String address) {
        Log.d(TAG, "onDeviceSelected: address = " + address);
        controlledDevice = address;
        if (controlledDevice != null) {
            processIntent(shareIntent);
            shareIntent = null;
            showRemoteFragment(getSupportFragmentManager());
        }
    }

    /**
     * Обработать интент для шаринга
     *
     * @param intent
     */
    private void processIntent(Intent intent) {
        Log.d(TAG, "processIntent: " + intent);
        if (intent == null) return;

        String action = intent.getAction();
        String type = intent.getType();
        Bundle extras = intent.getExtras();
        String url = extras != null ? extras.getString(Intent.EXTRA_TEXT) : "";

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendImage(intent); // Handle single image being sent
            } else if (type.startsWith("audio/")) {
                handleSendAudio(intent);
            }
            /*
            else if (type.startsWith("text/") && isYouTube(url)) {
                handleSendYoutubeUrl(extractYoutubeId(url));
            }
            */
        } else if (Intent.ACTION_VIEW.equals(action)) {
            /*
            if (isYouTube(intent.getDataString())) {
                handleSendYoutubeUrl(extractYoutubeId(intent.getDataString()));
            } else {
                handleSendUrl(intent);
            }
            */
        }
    }

    /**
     * Обработка отправки изображения
     *
     * @param intent
     */
    private void handleSendImage(final Intent intent) {
        Log.d(TAG, "handleSendImage: " + intent);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Log.d(TAG, "doInBackground: " + intent);
                Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (imageUri != null) {
                    shareType = SHARE_TYPE_IMAGE;
                    File file = FileUtils.getRealPathFromUri(MainActivity.this, imageUri);
                    rcService.sendMessage(controlledDevice,
                            new SendFileCmd(SendFileCmd.CURRENT_IMAGE,
                                    file.getName(),
                                    FileUtils.readFile(file.getAbsoluteFile()))
                    );
                }

                return null;
            }
        }.execute();
    }

    /**
     * Отправка музыки
     *
     * @param intent
     */
    private void handleSendAudio(final Intent intent) {
        Log.d(TAG, "handleSendAudio: " + intent);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Log.d(TAG, "doInBackground: sending audio " + intent);
                Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                if (imageUri != null) {
                    shareType = SHARE_TYPE_AUDIO;
                    File file = FileUtils.getRealPathFromUri(MainActivity.this, imageUri);
                    Log.d(TAG, "doInBackground: file name = " + file.getName());
                    //TODO: прочитать ID3 тэг
                    rcService.sendMessage(controlledDevice,
                            new SendFileCmd(SendFileCmd.AUDIO,
                                    file.getName(),
                                    "unknown artist",
                                    file.getName(),
                                    "music",
                                    FileUtils.readFile(file.getAbsoluteFile()))
                    );
                }
                Log.d(TAG, "doInBackground: sent");

                return null;
            }
        }.execute();
    }
}
