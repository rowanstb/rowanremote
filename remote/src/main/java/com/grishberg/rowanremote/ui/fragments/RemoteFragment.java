package com.grishberg.rowanremote.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.grishberg.rowancommon.KeysConst;
import com.grishberg.rowancommon.data.models.commands.PlayerCmd;
import com.grishberg.rowancommon.interfaces.BoundListener;
import com.grishberg.rowancommon.interfaces.HandlerReceiver;
import com.grishberg.rowanremote.R;
import com.grishberg.rowanremote.common.interfaces.DevicesInteractor;
import com.grishberg.rowancommon.data.models.commands.KeyPressCmd;
import com.grishberg.rowanremote.data.networking.RcService;

/**
 * Фрагмент с пультом управления
 */
public class RemoteFragment extends Fragment implements BoundListener, View.OnTouchListener, View.OnClickListener {

    private Button btUp;
    private Button btDown;
    private Button btLeft;
    private Button btRight;
    private Button btSelect;
    private Button btBack;
    private Button btHome;

    private Button btPrev;
    private Button btPlay;
    private Button btPause;
    private Button btStop;
    private Button btNext;

    private DevicesInteractor<RcService> mListener;
    private String controlledDevice;

    public RemoteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RemoteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RemoteFragment newInstance() {
        RemoteFragment fragment = new RemoteFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_remote, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btBack = (Button) view.findViewById(R.id.btBack);
        btHome = (Button) view.findViewById(R.id.btHome);
        btSelect = (Button) view.findViewById(R.id.btSelect);
        btUp = (Button) view.findViewById(R.id.btUp);
        btDown = (Button) view.findViewById(R.id.btDown);
        btLeft = (Button) view.findViewById(R.id.btLeft);
        btRight = (Button) view.findViewById(R.id.btRight);

        btPrev = (Button) view.findViewById(R.id.btPrev);
        btPlay = (Button) view.findViewById(R.id.btPlay);
        btPause = (Button) view.findViewById(R.id.btPause);
        btStop = (Button) view.findViewById(R.id.btStop);
        btNext = (Button) view.findViewById(R.id.btNext);

        btBack.setOnTouchListener(this);
        btHome.setOnTouchListener(this);
        btSelect.setOnTouchListener(this);
        btUp.setOnTouchListener(this);
        btDown.setOnTouchListener(this);
        btLeft.setOnTouchListener(this);
        btRight.setOnTouchListener(this);

        btPrev.setOnClickListener(this);
        btPlay.setOnClickListener(this);
        btPause.setOnClickListener(this);
        btStop.setOnClickListener(this);
        btNext.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DevicesInteractor) {
            mListener = (DevicesInteractor) context;
            mListener.register(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.unregister(this);
        mListener = null;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        RcService service = mListener.getService();
        if (service == null) {
            Toast.makeText(getContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(controlledDevice)) {
            Toast.makeText(getContext(), R.string.no_device, Toast.LENGTH_SHORT).show();
            return false;
        }
        boolean isPressed = false;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // нажатие
                isPressed = true;
                break;
            case MotionEvent.ACTION_MOVE: // движение
                return false;
            case MotionEvent.ACTION_UP: // отпускание
            case MotionEvent.ACTION_CANCEL:
                isPressed = false;
                break;
        }
        switch (v.getId()) {
            case R.id.btBack:
                service.sendMessage(controlledDevice, new KeyPressCmd(KeysConst.KEY_ESC, isPressed));
                return true;
            case R.id.btHome:
                service.sendMessage(controlledDevice, new KeyPressCmd(KeysConst.KEY_HOME, isPressed));
                return true;
            case R.id.btSelect:
                service.sendMessage(controlledDevice, new KeyPressCmd(KeysConst.KEY_ENTER, isPressed));
                return true;

            case R.id.btUp:
                service.sendMessage(controlledDevice,
                        new KeyPressCmd(KeysConst.KEY_UP, isPressed));
                return true;

            case R.id.btDown:
                service.sendMessage(controlledDevice,
                        new KeyPressCmd(KeysConst.KEY_DOWN, isPressed));
                return true;

            case R.id.btLeft:
                service.sendMessage(controlledDevice,
                        new KeyPressCmd(KeysConst.KEY_LEFT, isPressed));
                return true;

            case R.id.btRight:
                service.sendMessage(controlledDevice,
                        new KeyPressCmd(KeysConst.KEY_RIGHT, isPressed));
                return true;

        }
        return false;
    }

    @Override
    public void onServiceBound(HandlerReceiver service) {

    }

    @Override
    public void onServiceUnBound(HandlerReceiver service) {

    }

    @Override
    public void onMessageReceived(int what, Object obj) {

    }

    public void setControlledDevice(String controlledDevice) {
        this.controlledDevice = controlledDevice;
    }

    @Override
    public void onClick(View v) {
        RcService service = mListener.getService();
        if (service == null) {
            Toast.makeText(getContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(controlledDevice)) {
            Toast.makeText(getContext(), R.string.no_device, Toast.LENGTH_SHORT).show();
            return;
        }
        switch (v.getId()) {
            // кнопки управления плеером
            case R.id.btPrev:
                service.sendMessage(controlledDevice,
                        new PlayerCmd(PlayerCmd.CMD_PREV));
                return ;
            case R.id.btPlay:
                service.sendMessage(controlledDevice,
                        new PlayerCmd(PlayerCmd.CMD_PLAY));
                return ;
            case R.id.btPause:
                service.sendMessage(controlledDevice,
                        new PlayerCmd(PlayerCmd.CMD_PAUSE));
                return ;
            case R.id.btStop:
                service.sendMessage(controlledDevice,
                        new PlayerCmd(PlayerCmd.CMD_STOP));
                return ;
            case R.id.btNext:
                service.sendMessage(controlledDevice,
                        new PlayerCmd(PlayerCmd.CMD_NEXT));
                return ;
        }
    }
}
