package com.grishberg.rowanremote.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grishberg.rowancommon.interfaces.BoundListener;
import com.grishberg.rowancommon.interfaces.HandlerReceiver;
import com.grishberg.rowanremote.R;
import com.grishberg.rowanremote.common.interfaces.DevicesInteractor;
import com.grishberg.rowanremote.common.interfaces.OnItemClickListener;
import com.grishberg.rowanremote.data.models.ServerInfoContainer;
import com.grishberg.rowanremote.data.networking.RcService;
import com.grishberg.rowanremote.ui.controllers.DevicesAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link DevicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DevicesFragment extends Fragment implements OnItemClickListener, BoundListener {
    private static final String TAG = DevicesFragment.class.getSimpleName();

    private RecyclerView rvDevices;
    private DevicesAdapter adapter;
    private List<ServerInfoContainer> devices = Collections.synchronizedList(new ArrayList<ServerInfoContainer>());

    private DevicesInteractor<RcService> mListener;
    private Handler searchHandler;

    public DevicesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DevicesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DevicesFragment newInstance() {
        DevicesFragment fragment = new DevicesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        adapter = new DevicesAdapter(devices, this);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_devices, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated: ");
        super.onViewCreated(view, savedInstanceState);
        rvDevices = (RecyclerView) view.findViewById(R.id.rvDevices);
        rvDevices.setVisibility(View.VISIBLE);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rvDevices.setLayoutManager(llm);
        rvDevices.setAdapter(adapter);
        if (mListener.getService() != null) {
            mListener.getService().startListeningServers();
            mListener.getService().findServers();
            searchHandler = new Handler();
            searchHandler.postDelayed(updateDeviceListRunnable, 5000);
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: ");
        super.onAttach(context);
        if (context instanceof DevicesInteractor) {
            mListener = (DevicesInteractor) context;
            mListener.register(this);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDeviceFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach: ");
        super.onDetach();
        if (mListener.getService() != null) {
            mListener.getService().stopListeningServers();
        }
        if (searchHandler != null) {
            searchHandler.removeCallbacks(updateDeviceListRunnable);
        }
        mListener.unregister(this);
        mListener = null;
    }

    @Override
    public void onItemClicked(long id, int offsetTop, int offsetBottom) {
        if (mListener != null) {
            mListener.getService().connectToServer(devices.get((int) id).getAddress());
            mListener.onDeviceSelected(devices.get((int) id).getAddress());
        }
    }

    @Override
    public void onServiceBound(HandlerReceiver service) {
        if (searchHandler != null) return;
        if (mListener.getService() != null) {
            mListener.getService().startListeningServers();
            mListener.getService().findServers();
            searchHandler = new Handler();
            searchHandler.postDelayed(updateDeviceListRunnable, 5000);
        }
    }

    @Override
    public void onServiceUnBound(HandlerReceiver service) {

    }

    /**
     * Сообщение от сервера
     * @param what
     * @param obj
     */
    @Override
    public void onMessageReceived(int what, Object obj) {
        if (what == RcService.ACTION_FOUND_SERVER) {
            ServerInfoContainer serverInfo = (ServerInfoContainer) obj;
            if (!devices.contains(serverInfo)) {
                Log.d(TAG, "onMessageReceived: on server found: " + obj);
                devices.add(serverInfo);
                adapter.notifyDataSetChanged();
            }
        }
    }

    private Runnable updateDeviceListRunnable = new Runnable() {
        @Override
        public void run() {
            if (mListener.getService() != null) {
                mListener.getService().findServers();
            }
            searchHandler.postDelayed(updateDeviceListRunnable, 10000);
        }
    };
}
