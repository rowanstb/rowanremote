package com.grishberg.rowanremote.ui.controllers;

/**
 * Created by grishberg on 22.06.16.
 */
public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}
