package com.grishberg.rowanremote;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by grishberg on 14.05.16.
 */
public class App extends Application {
    private static final String TAG = App.class.getSimpleName();
    private static int activitiesCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "RowanRemote onCreate: v." + BuildConfig.VERSION_NAME);
        initFabric(getApplicationContext());
    }

    /**
     * Инициализация крашлитикса
     * @param applicationContext
     */
    private void initFabric(Context applicationContext) {
        Fabric.with(applicationContext, new Crashlytics());
    }

    /**
     * Вызвать когда активностьь создается
     */
    public static void activityCreated() {
        activitiesCount++;
    }

    /**
     * Вызвать когда активность уничтожается
     */
    public static void activityDestroyed() {
        activitiesCount--;
    }

    public static boolean isExitingFromApp() {
        return activitiesCount == 0;
    }
}
