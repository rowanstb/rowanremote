package com.grishberg.rowanremote;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;

import com.grishberg.rowancommon.data.models.commands.KeyPressCmd;
import com.grishberg.rowanremote.data.networking.RcService;
import com.grishberg.rowanremote.ui.activities.MainActivity;
import com.grishberg.utils.network.ConnectionReceiver;
import com.grishberg.utils.network.ConnectionReceiverImpl;
import com.grishberg.utils.network.interfaces.OnAcceptedListener;
import com.grishberg.utils.network.interfaces.OnConnectionErrorListener;
import com.grishberg.utils.network.interfaces.OnMessageListener;
import com.grishberg.utils.network.interfaces.OnServerConnectionEstablishedListener;
import com.grishberg.utils.network.tcp.server.TcpServer;
import com.grishberg.utils.network.tcp.server.TcpServerImpl;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class ApplicationTest extends ActivityInstrumentationTestCase2<MainActivity> {
    public static final int UDP_PORT = 5001;
    public static final int TIMEOUT = 100;
    public static final int BACK_TCP_PORT = 5002;
    public static final int PORT = 5003;

    private MainActivity activity;
    private boolean isSuccessFoundServer;
    private boolean isSuccessReceived;
    private boolean isSuccesConnectedToServer;
    private ConnectionReceiver connectionReceiver;
    private TcpServer tcpServer;
    private CountDownLatch signalFoundServers = new CountDownLatch(1);
    private CountDownLatch signalConnectedToServer = new CountDownLatch(1);
    private CountDownLatch signalReceivedMessageFromServers = new CountDownLatch(1);
    private boolean serverMessageReceived;
    private int serverReceivedCount;
    private String serverAddress;
    private String clientAddress;
    private Parcelable receivedCmd;

    public ApplicationTest() {
        super(MainActivity.class);
    }

    @Rule
    public final ServiceTestRule mServiceRule = new ServiceTestRule();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();
    }

    @Test
    public void testWithBoundService() throws Exception {
        //init connection receiver
        startConnectionReceiver();
        startServer();

        isSuccessFoundServer = false;
        isSuccesConnectedToServer = false;
        isSuccessReceived = false;
        IBinder binder = mServiceRule.bindService(
                new Intent(InstrumentationRegistry.getTargetContext(), RcService.class));
        RcService service = (RcService) ((RcService.LocalBinder) binder).getService();
        assertNotNull(service);
        service.registerHandler(handler);
        service.findServers();
        signalFoundServers.await(5, TimeUnit.SECONDS);
        assertTrue("Server not found", isSuccessFoundServer);
        assertNotNull("server address is null", serverAddress);
        service.connectToServer(serverAddress);
        signalConnectedToServer.await(5, TimeUnit.SECONDS);
        assertTrue("not connected to server", isSuccesConnectedToServer);
        service.sendMessage(serverAddress, new KeyPressCmd(KeyEvent.KEYCODE_DPAD_LEFT, true));
        signalReceivedMessageFromServers.await(5, TimeUnit.SECONDS);
        assertNotNull("Cmd is null", receivedCmd);
        receivedCmd = null;
        // unregister and re register handler
        service.unregisterHandler(handler);
        service.sendMessage(serverAddress, new KeyPressCmd(KeyEvent.KEYCODE_DPAD_RIGHT, true));
        Thread.sleep(1000);
        signalReceivedMessageFromServers = new CountDownLatch(1);
        service.registerHandler(handler);
        signalReceivedMessageFromServers.await(5, TimeUnit.SECONDS);
        assertNotNull("Cmd is null", receivedCmd);
        assertTrue("received wrong cmd type", receivedCmd instanceof KeyPressCmd);
        assertTrue(String.format(Locale.US, "received wrong cmd parameter = %d", ((KeyPressCmd) receivedCmd).getKey()),
                ((KeyPressCmd) receivedCmd).getKey() == KeyEvent.KEYCODE_DPAD_RIGHT);
        stopConnectionReceiver();
        stopServer();
    }

    private void startConnectionReceiver() {
        final CountDownLatch broadcastConnectionSignal = new CountDownLatch(1);
        connectionReceiver = new ConnectionReceiverImpl(UDP_PORT, BACK_TCP_PORT);
        connectionReceiver.setConnectionListener(new OnServerConnectionEstablishedListener() {
            @Override
            public void onConnectionEstablished(String address) {
                isSuccessReceived = true;
                broadcastConnectionSignal.countDown();
            }
        });

        connectionReceiver.setErrorListener(new OnConnectionErrorListener() {
            @Override
            public void onError(Throwable t) {
                assertTrue(t.getMessage(), false);
                broadcastConnectionSignal.countDown();
            }
        });
        // listen incoming broadcast connection
        connectionReceiver.start();
    }

    private void stopConnectionReceiver() {
        connectionReceiver.stop();
    }

    private void startServer() throws Exception {
        tcpServer = new TcpServerImpl(PORT, new OnMessageListener() {
            @Override
            public void onReceivedMessage(String address, byte[] message) {
                serverMessageReceived = true;
                // send back message
                tcpServer.sendMessage(address, message);
                serverReceivedCount++;
            }
        }, new OnAcceptedListener() {
            @Override
            public void onAccepted(String address) {
                clientAddress = address;
                isSuccesConnectedToServer = true;
                signalConnectedToServer.countDown();
            }
        });
        tcpServer.start();
    }

    private void stopServer() {
        tcpServer.stop();
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RcService.ACTION_FOUND_SERVER:
                    isSuccessFoundServer = true;
                    serverAddress = (String) msg.obj;
                    signalFoundServers.countDown();
                    break;
                case RcService.ACTION_CONNECTED_TO_SERVER:
                    break;
                case RcService.ACTION_MESSAGE:
                    isSuccessReceived = true;
                    receivedCmd = (Parcelable) msg.obj;
                    signalReceivedMessageFromServers.countDown();
                    break;
            }
        }
    };
}